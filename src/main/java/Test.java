import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Test extends BaseTest {

    public static void main(String[] args) {
        EventFiringWebDriver driver = getConfiguredDriver();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.fillEmailInput();
        loginPage.fillPasswordInput();
        loginPage.clickLoginButton();

        CategoryPage categoryPage = new CategoryPage(driver);
        categoryPage.clickOnCategorySubItem();
        categoryPage.addNewCategory();
        categoryPage.filterCategoryTableByName();
        categoryPage.clicklogOutImage();
        categoryPage.clicklogOutButton();
        quitDriver(driver);
    }
}

