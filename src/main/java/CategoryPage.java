import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CategoryPage {
    private EventFiringWebDriver driver;
    private By catalogMenuItem = By.id("subtab-AdminCatalog");
    private By categoryItem = By.id("subtab-AdminCategories");
    private By topAddCategoryButton = By.id("page-header-desc-category-new_category");
    private By nameField = By.id("name_1");
    private By saveButton = By.className("process-icon-save");
    private By checkThatMessageIsAppeared = By.cssSelector("div > div[class='alert alert-success']");
    private By filterCategoriesByNameButton = By.xpath("//th[3]/span/a[2]");
    private By categoryItemNamesList = By.cssSelector("* > td:nth-child(3)");
    private String CreatedCategoryName = "001";
    private By logOutImage = By.className("employee_avatar_small");
    private By logOutButton = By.cssSelector("[id=\"header_logout\"]");
    private By salesScoreElement = By.id("sales_score");

    public CategoryPage (EventFiringWebDriver driver) {this.driver = driver;}

    public void open() {driver.get(MyProperties.getBaseAdminUrl());}

    public void clickOnCategorySubItem() {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(catalogMenuItem));
        WebDriverWait waitForSalesScoreElement = new WebDriverWait(driver, 10);
        waitForSalesScoreElement.until(ExpectedConditions.elementToBeClickable(salesScoreElement));
        Actions act = new Actions(driver);
        act.moveToElement(driver.findElement(catalogMenuItem)).perform();
        act.perform();
        act.build();
        act.perform();
        WebDriverWait waitForSubCatalogMenuItem = new WebDriverWait(driver, 10);
        waitForSubCatalogMenuItem.until(ExpectedConditions.elementToBeClickable(categoryItem));
        driver.findElement(categoryItem).click();
    }

    public void addNewCategory() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(topAddCategoryButton));
        driver.findElement(topAddCategoryButton).click();
        WebDriverWait WaitForNameField = new WebDriverWait(driver, 10);
        WaitForNameField.until(ExpectedConditions.presenceOfElementLocated(nameField));
        driver.findElement(nameField).sendKeys(CreatedCategoryName);
        driver.findElement(saveButton).click();
        WebDriverWait WaitForMessageIsAppeared = new WebDriverWait(driver, 10);
        WaitForMessageIsAppeared.until(ExpectedConditions.presenceOfElementLocated(checkThatMessageIsAppeared));
        driver.findElement(checkThatMessageIsAppeared);
        String CreatedMessage = driver.findElement(checkThatMessageIsAppeared).getText().replaceAll("[^A-Za-zА-Яа-я0-9]", "");
        System.out.println("The \"" + CreatedMessage + "\" message is displayed on the page.");
    }

    public void filterCategoryTableByName() {
        driver.findElement(filterCategoriesByNameButton).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(logOutImage));
        driver.findElement(filterCategoriesByNameButton).click();
        WebDriverWait WaitForCategoryItemNames = new WebDriverWait(driver, 10);
        WaitForCategoryItemNames.until(ExpectedConditions.presenceOfElementLocated(categoryItemNamesList));
        List<WebElement> CategoryItemNames = driver.findElements(categoryItemNamesList);
        for(int i = 0; i < CategoryItemNames.size(); i++){
            if(CategoryItemNames.get(i).getText().equals(CreatedCategoryName)) {
                System.out.println("The \"" + CategoryItemNames.get(i).getText() + "\" category was created successfully.");
                break;
            }
        }
    }

    public void clicklogOutImage() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(logOutImage));
        driver.findElement(logOutImage).click();
    }

    public void clicklogOutButton() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(logOutButton));
        driver.findElement(logOutButton).click();
    }
}
