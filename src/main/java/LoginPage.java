import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;


public class LoginPage {
    private EventFiringWebDriver driver;
    private By emailInput = By.cssSelector("input[name='email']");
    private By passwordInput = By.id("passwd");
    private By loginButton = By.name("submitLogin");
    private String email = "webinar.test@gmail.com";
    private String password = "Xcg7299bnSmMuRLp9ITw";

    public LoginPage (EventFiringWebDriver driver) {this.driver = driver;}

    public void open() {driver.get(MyProperties.getBaseAdminUrl());}

    public void fillEmailInput() {driver.findElement(emailInput).sendKeys(email);}

    public void fillPasswordInput() {driver.findElement(passwordInput).sendKeys(password);}

    public void clickLoginButton() {driver.findElement(loginButton).click();}


}
