import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.concurrent.TimeUnit;

public abstract class BaseTest {
    public static WebDriver getDriver(){
        WebDriver driver;
        String browser = MyProperties.getBrowser();
        switch (browser) {
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(BaseTest.class.getResource("geckodriver.exe").getFile()).getPath());
                    driver = new FirefoxDriver();
                    break;
            case "iexplore":
            case  "internet explorer":
                System.setProperty(
                        "webdriver.ie.driver",
                        new File(BaseTest.class.getResource("IEDriverServer.exe").getFile()).getPath());
                driver = new InternetExplorerDriver();
                break;
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(BaseTest.class.getResource("chromedriver.exe").getFile()).getPath());
                driver = new ChromeDriver();
                break;
        }
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
        return driver;
    }

    public static EventFiringWebDriver getConfiguredDriver(){
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);
        wrappedDriver.register(new MyEventHandler());
        return wrappedDriver;
    }

    public static void quitDriver(WebDriver driver) {
        WebDriverWait WaitForLogOut = new WebDriverWait(driver, 10);
        WaitForLogOut.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input[name='email']")));
        driver.quit();}
}
